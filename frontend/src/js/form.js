function sendFromAjax(e, success, error) {
    e.preventDefault();
    var $form = $(e.target);
    var url = $form.attr("action");
    var data = $form.serialize();
    var method = $form.attr('method') || "POST";
    var token = $('input[name="csrfmiddlewaretoken"]').prop('value');
    var sessionValue = $("#hdnSession").data('value');
    console.log(sessionValue)
    $.ajax({
        url: url,
        method: method,
        data: data,
        headers:{"X-CSRFToken": token,
                 "Authorization": sessionValue},
        success: success,
        error: error
    });
}
$(document).ready(function () {
    $('.jsRegistrationForm').on('submit', function(e) {
        e.preventDefault();
        var url = $(this).attr('action');
        sendFromAjax(e, function(response) {
            if(response.success) {
                $('.form')[0].reset();
                $('input, textarea', 'select',).trigger("refresh");
            } else {
                $('.form__error').remove();
                let er = response.error
                for(var key in er) {
                    var error = er[key];
                    console.log(er)
                }
            }
        });
    });
    $('.jsLoginForm').on('submit', function(e) {
        e.preventDefault();
        var sessionValue = $("#hdnSession").data('value');
        console.log(sessionValue)
        var url = $(this).attr('action');
        sendFromAjax(e, function(response) {
            if(response.success) {
                $('.form')[0].reset();
                $('input, textarea', 'select',).trigger("refresh");
            } else {
                $('.form__error').remove();
                let er = response.error
                for(var key in er) {
                    var error = er[key];
                    console.log(er)
                }
            }
        });
    });
    $('.jsAddVehicleForm').on('submit', function(e) {
        e.preventDefault();
        var url = $(this).attr('action');
        sendFromAjax(e, function(response) {
            if(response.success) {
                $('.form')[0].reset();
                $('input, textarea', 'select',).trigger("refresh");
            } else {
                $('.form__error').remove();
                let er = response.error
                for(var key in er) {
                    var error = er[key];
                    console.log(er)
                }
            }
        });
    });
    $('.jsAddVehicleUserForm').on('submit', function(e) {
        e.preventDefault();
        var url = $(this).attr('action');
        sendFromAjax(e, function(response) {
            if(response.success) {
                $('.form')[0].reset();
                $('input, textarea', 'select',).trigger("refresh");
            } else {
                $('.form__error').remove();
                let er = response.error
                for(var key in er) {
                    var error = er[key];
                    console.log(er)
                }
            }
        });
    });
    $('.jsUserProfile').on('submit', function(e) {
        e.preventDefault();
        var url = $(this).attr('action');
        sendFromAjax(e, function(response) {
            if(response.success) {
                $('.form')[0].reset();
                $('input, textarea', 'select',).trigger("refresh");
            } else {
                $('.form__error').remove();
                let er = response.error
                for(var key in er) {
                    var error = er[key];
                    console.log(er)
                }
            }
        });
    });
});
