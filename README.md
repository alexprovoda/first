# Установка
Копируем файл `example.env` в файл `.env`

```bash
cp example.env .env
```

Редактируем файл `.env` и изменяем `SECRET_KEY` на любой набор символов, например, название сайта.
Все остальные параметры можно не трогать.

Устанавливаем виртуальное окружение

```bash
virtualenv -p python3.6 venv
```

Поднимаем базу данных и redis с помощью докера

```bash
docker-compose up -d
```

Активируем виртуальное окружение

```bash
source venv/bin/activate
```

Устанавливаем зависимости, применяем миграции

```bash
pip install -r requirements.txt

python backend/manage.py migrate
```

Создаем суперпользователя следующей командой и отвечаем на вопросы, которые задаст система

```bash
python backend/manage.py createsuperuser
```

Для наполнения базы данных тестовыми данными, выполнить команду
```bash
python backend/manage.py test_data
```

Запускаем сервер

```bash
python backend/manage.py runserver
```

Заходим в админку http://localhost:8000/admin/ с только что созданными логином и паролем.

Чтобы запустить `celery` (необходим для запуска задач по расписанию), используйте команду

```bash
celery -A app worker -B
```


