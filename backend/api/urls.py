from django.urls import include, path, re_path
from drf_yasg import openapi
from drf_yasg.views import get_schema_view
from rest_framework import permissions
from rest_framework.authtoken.views import ObtainAuthToken

from app import settings
from user.viewset.registration import RegistrationView
from user.viewset.user import UserViewSet
from vehicle.views import VehicleViews

schema_view = get_schema_view(
    openapi.Info(
        title="Vehicle API",
        default_version='v1',
        description="API for vehicle",
    ),
    url=settings.SWAGGER_BASE_URL,
    public=True,
    permission_classes=(permissions.AllowAny,),
)

auth_urlpatterns = [
    path('registration/', RegistrationView.as_view(), name='registration'),
    path('token_auth/', ObtainAuthToken.as_view(), name="token_auth"),
    path('user/', UserViewSet.as_view({'get': 'list', 'patch': 'update'}), name="user"),
]
vehicle_urlpatterns = [
    path('vehicle/', VehicleViews.as_view({'get': 'list', 'post': 'create', 'put': 'update', }), name='vehicle'),
]

urlpatterns = [
    path('auth/', include(auth_urlpatterns)),
    path('user_vehicles/', UserViewSet.as_view({'get': 'get_user_vehicles'})),
    path('add_user_vehicles/', VehicleViews.as_view({'post': 'add_user_vehicles'}), name='add_user_vehicles'),
    path('auto/', include(vehicle_urlpatterns)),
    re_path(r'^docs(?P<format>\.json|\.yaml)$', schema_view.without_ui(cache_timeout=0), name='schema-json'),
    re_path(r'^docs/$', schema_view.with_ui('swagger', cache_timeout=0), name='schema-swagger-ui'),
    re_path(r'^redoc/$', schema_view.with_ui('redoc', cache_timeout=0), name='schema-redoc'),
]
