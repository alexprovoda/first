from django.conf import settings
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import include, path, re_path
from django.views.static import serve

from user.views.cabinet import cabinet
from user.views.login import login_form, sign_in
from user.views.registration import home, registration
from vehicle.views import vehicle, catalog_vehicle

urlpatterns = [
    re_path(r'^i18n/', include('django.conf.urls.i18n')),
    path('admin/', admin.site.urls),
    path('admin/auth/', include('django.contrib.auth.urls')),
    re_path(r'^_nested_admin/', include('nested_admin.urls')),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('api/v1/', include('api.urls')),
    path('', home, name="home"),
    path('registration/', registration, name="user_registration"),
    path('sign_in/', sign_in, name="user_login"),
    path('login/', login_form, name="auth_login"),
    path('vehicle/', vehicle, name="add_vehicle"),
    path('catalog_vehicle/', catalog_vehicle, name="add_vehicle_user"),
    path('cabinet/', cabinet, name="user_cabinet"),
]

urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
urlpatterns += [
    re_path(r'^media/(?P<path>.*)$', serve, {'document_root': settings.MEDIA_ROOT}),
]
