VENDOR_APPS = [
    'solo',
    'drf_yasg',
    'fcm_django',
    'easy_thumbnails',
    'ckeditor',
    'mptt',
    'modeltranslation',
    'nested_admin',
    'rest_framework',
    'rest_framework.authtoken',
    'rest_framework_swagger',
    'snowpenguin.django.recaptcha3',
]

DJANGO_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sitemaps',
    'django.contrib.sites',
]

PROJECT_APPS = [
    'api',
    'vehicle',
    'user',
]

INSTALLED_APPS = (VENDOR_APPS + DJANGO_APPS + PROJECT_APPS)
