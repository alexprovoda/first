from django.contrib import admin
from fcm_django.models import FCMDevice

from ..models.user import User

admin.site.unregister(FCMDevice)


@admin.register(User)
class UserAdmin(admin.ModelAdmin):
    list_display = ['username', 'email', 'language']
    exclude = ['password']
