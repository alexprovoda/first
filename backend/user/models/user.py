from django.contrib.auth.models import AbstractUser
from django.db import models

from user.fields import UserLanguageField
from user.modeldata.language_choice import UserLanguageChoice
from vehicle.models import Vehicle


class User(AbstractUser):
    language = UserLanguageField(max_length=64, choices=UserLanguageChoice.choices(),
                                 default=UserLanguageChoice.NOT_SELECTED, null=True, blank=True, verbose_name='Язык')
    vehicles = models.ManyToManyField(Vehicle, related_name='user_vehicles', blank=True, verbose_name='Транспорт')

    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.get_full_name() if (self.first_name and self.last_name) \
            else self.email if self.email \
            else self.username
