import random

from django.core.management import BaseCommand

from user.models import User
from vehicle.models import Vehicle


class Command(BaseCommand):
    args = '<foo bar ...>'
    help = 'our help string comes here'

    names = [
        "Oliver", "George", "Harry", "Jack", "Jacob", "Noah", "Charlie", "Muhammad", "Thomas", "Oscar", "William",
        "James", "Henry", "Leo", "Alfie", "Joshua", "Freddie", "Archie", "Ethan", "Isaac", "Alexander", "Joseph",
        "Edward", "Samuel", "Max", "Daniel", "Arthur", "Lucas", "Mohammed", "Logan", "Theo", "Harrison", "Benjamin",
        "Mason", "Sebastian", "Finley", "Adam", "Dylan", "Zachary", "Riley", "Teddy", "Theodore", "David", "Toby",
        "Jake", "Louie", "Elijah", "Reuben", "Arlo", "Hugo", "Luca", "Jaxon", "Matthew", "Harvey", "Reggie", "Michael",
        "Harley", "Jude", "Albert", "Tommy", "Luke", "Stanley", "Jenson", "Frankie", "Jayden", "Gabriel", "Elliot",
        "Mohammad", "Ronnie", "Charles", "Louis", "Elliott", "Frederick", "Nathan", "Lewis", "Blake", "Rory", "Ollie",
        "Ryan", "Tyler", "Jackson", "Dexter", "Alex", "Austin", "Kai", "Albie", "Caleb", "Carter", "Bobby", "Ezra",
        "Ellis", "Leon", "Roman", "Ibrahim", "Aaron", "Liam", "Jesse", "Jasper", "Felix", "Jamie"
    ]
    first_names = names
    last_names = [
        "Nielsen", "Jensen", "Hansen", "Pedersen", "Andersen", "Christensen", "Larsen", "Sorensen",
        "Rasmussen",
        "Jorgensen", "Petersen", "Madsen", "Kristensen", "Olsen", "Thomsen", "Christiansen", "Poulsen",
        "Johansen", "Moller", "Mortensen", "Smith", "Jones", "Taylor", "Brown", "William", "Wilson", "Johnson",
        "Davies", "Robinso", "Wright",
        "Thompso", "Evans", "Walker", "White", "Roberts", "Green", "Hall", "Wood", "Jackson", "Clarke",
        "Brown",
        "Smith", "Patel", "Jones", "Williams", "Johnson", "Taylor", "Thomas", "Roberts", "Khan", "Lewis",
        "Jackson", "Clarke", "James", "Phillips", "Wilson", "Ali", "Mason", "Mitchell", "Rose", "Davis",
        "Davies", "Cox", "Alexander", "Martin", "Bernard", "Dubois", "Thomas", "Robert", "Richard", "Petit",
        "Durand", "Leroy", "Moreau",
        "Simon", "Laurent", "Lefebvre", "Michel", "Garcia", "David", "Bertrand", "Roux", "Vincent", "Fournier",
        "Morel", "Girard", "André", "Lefèvre", "Mercier", "Dupont", "Lambert", "Bonnet", "François",
        "Martinez", "Rossi", "Russo", "Ferrari", "Esposito", "Bianchi", "Romano", "Colombo", "Bruno", "Ricci",
        "Greco",
        "Marino", "Gallo", "De Luca", "Conti", "Costa", "Mancini", "Giordano", "Rizzo", "Lombardi", "Barbieri",
        "Moretti", "Fontana", "Caruso", "Mariani", "Ferrara", "Santoro", "Rinaldi", "Leone", "D'Angelo",
        "Longo", "De Jong", "De Vries", "Van den Berg", "Van Dijk", "Van Dyk", "Bakker", "Visser", "Smit",
        "Meijer",
        "Meyer", "De Boer", "Mulder", "De Groot", "Bos", "Vos", "Peters", "Hendriks", "Van Leeuwen", "Dekker",
        "Brouwer", "De Wit", "Dijkstra", "Smits", "De Graaf", "Van der Meer"
    ]

    names = {'Audi': 'Ауди', 'BMW': 'БМВ', 'Infiniti': 'Инфинити', 'Mercedes-Benz': 'Мерседес-Бенц', 'Nissan': 'Ниссан',
             'Toyota': 'Тойота', 'Volvo': 'Вольво'}
    created_at = [2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, ]

    def get_rand_user_params(self):
        first_name = random.choice(self.first_names)
        last_name = random.choice(self.last_names)
        email = "++++" + first_name.lower() + last_name.lower() + str(random.randint(1, 999)) + "@gmail.com"

        return (email, first_name, last_name)

    def __add_fake_user(self, email, first_name, last_name, password="7634001A"):
        User.objects.create_user(username=email,
                                 email=email,
                                 password=password,
                                 first_name=first_name,
                                 last_name=last_name,
                                 is_active=True)

    def get_rand_vehicle_params(self):
        name_en = random.choice(list(self.names.keys()))
        name_ru = self.names.get(name_en)
        created_at = random.choice(self.created_at)
        return (name_ru, name_en, created_at)

    def __add_fake_vehicle(self, name_ru, name_en, created_at):
        Vehicle.objects.create(name_ru=name_ru,
                               name_en=name_en,
                               created_at=created_at)

    def generate_fake_user(self):
        (email, first_name, last_name) = self.get_rand_user_params()
        return self.__add_fake_user(email, first_name, last_name)

    def generate_fake_vehicle(self):
        (name_ru, name_en, created_at) = self.get_rand_vehicle_params()
        return self.__add_fake_vehicle(name_ru, name_en, created_at)

    def add_arguments(self, parser):
        parser.add_argument('-e', '--email', type=str, nargs='?')
        parser.add_argument('-c', '--count', type=int, nargs='?')

    def handle(self, *args, **options):
        inv_count = 25
        if options['count']:
            inv_count = int(options['count'])
        for i in range(inv_count):
            self.generate_fake_user()
            self.generate_fake_vehicle()
