# Generated by Django 2.2.16 on 2021-04-05 13:03

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('user', '0005_auto_20210403_1329'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='vehicles',
            field=models.ManyToManyField(blank=True, related_name='user_vehicles', to='vehicle.Vehicle', verbose_name='Транспорт'),
        ),
    ]
