from django.http import JsonResponse
from drf_yasg.utils import swagger_auto_schema
from rest_framework import viewsets, filters, permissions
from rest_framework.decorators import action
from rest_framework.response import Response

from user.models import User
from user.serializers.user import UpdateUserSerializer, UserSerializer
from vehicle.models import Vehicle
from vehicle.serializers import VehicleSerializer


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    # serializer_class = UserSerializer
    filter_backends = (filters.SearchFilter,)
    search_fields = ('username',)

    def get_serializer_class(self):
        if self.action == 'update':
            return UpdateUserSerializer
        return UserSerializer

    def get_object(self):
        return self.request.user

    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer_class()
        data = serializer(instance=instance, data=request.data, partial=True)
        if data.is_valid(raise_exception=True):
            data.save()
            return Response({'status': True, 'data': data.data})
        return Response({'status': False, 'error': data.errors})

    @swagger_auto_schema(tags=['user', ])
    @action(detail=False, methods=['GET', ], serializer_class=VehicleSerializer,
            permission_classes=(permissions.IsAuthenticated,), name='user_vehicles')
    def get_user_vehicles(self, *args, **kwargs):
        user = self.request.user
        if user.is_authenticated:
            vehicle = Vehicle.objects.filter(user_vehicles=user)
            result = self.serializer_class(vehicle, many=True)
            return JsonResponse({'vehicle': result})
        else:
            return JsonResponse({'error': 'Not Authenticated'})
