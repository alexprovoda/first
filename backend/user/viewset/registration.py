from drf_yasg.utils import swagger_auto_schema
from rest_framework import generics, status
from rest_framework.response import Response

from user.serializers.registration import RegistrationSerializer


class RegistrationView(generics.GenericAPIView):
    serializer_class = RegistrationSerializer
    permission_classes = ()

    @swagger_auto_schema(tags=['auth', ])
    def post(self, request, *args, **kwargs):
        serializer = RegistrationSerializer(data=request.data)
        if serializer.is_valid():
            user = serializer.save()

            return Response({'status': True}, status=status.HTTP_200_OK)
        else:
            try:
                error = str(serializer.errors).split('string=')[1]
                error = error.split('\'')[1]
                return Response({'status': False, 'error': error}, status=status.HTTP_403_FORBIDDEN)
            except Exception as e:
                return Response({'status': False, 'error': 'Something is going wrong...'},
                                status=status.HTTP_400_BAD_REQUEST)
