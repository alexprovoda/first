from user.modeldata.language_choice import UserLanguageChoice
from utils.enum_fields import EnumField


class UserLanguageField(EnumField):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs, clazz=UserLanguageChoice)
