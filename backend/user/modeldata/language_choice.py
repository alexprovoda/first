from utils.choice_enum import ChoiceEnum


class UserLanguageChoice(ChoiceEnum):
    NOT_SELECTED = ('NOT_SELECTED', 'Language not selected', 'Язык не выбран')
    RUSSIAN = ('RUSSIAN', 'Russian language selected', 'Выбран русский язык')
    ENGLISH = ('ENGLISH', 'English language selected', 'Выбран английский язык')

    def __init__(self, code, fullname, fullname_ru):
        self.code = code
        self.fullname = fullname
        self.fullname_ru = fullname_ru
