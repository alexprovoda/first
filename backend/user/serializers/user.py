from rest_framework import serializers

from user.modeldata.language_choice import UserLanguageChoice
from user.models import User


class EnumSerializerField(serializers.ChoiceField):
    def __init__(self, enum, **kwargs):
        self.enum = enum
        kwargs['choices'] = [(e.name, e.name) for e in enum]
        super(EnumSerializerField, self).__init__(**kwargs)

    def to_representation(self, obj):
        if type(obj) == str:
            return obj
        return obj.code

    def to_internal_value(self, data):
        try:
            return self.enum[data]
        except KeyError:
            self.fail('invalid_choice', input=data)


class UserSerializer(serializers.ModelSerializer):
    language = EnumSerializerField(enum=UserLanguageChoice)

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'email',
            'first_name',
            'last_name',
            'language',
            'vehicles',
        )
        read_only = (
            'username',
        )
        extra_kwargs = {
            'password': {'write_only': True},
        }


class UpdateUserSerializer(serializers.ModelSerializer):
    email_to = serializers.EmailField(required=False)
    language = EnumSerializerField(enum=UserLanguageChoice)

    def validate_email_to(self, value):
        if self.instance.email != value:
            _user = User.objects.filter(email=value).first()
            if _user is not None:
                raise serializers.ValidationError('Данный E-mail уже используется.')
        return value

    class Meta:
        model = User
        fields = (
            'id',
            'username',
            'first_name',
            'last_name',
            'email_to',
            'language'
        )
        read_only = (
            'username',
        )
        extra_kwargs = {
            'password': {'write_only': True},
        }
