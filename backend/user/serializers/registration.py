import django.contrib.auth.password_validation as validators
from django.core import exceptions
from rest_framework import serializers

from user.models import User


class RegistrationSerializer(serializers.ModelSerializer):
    password_confirm = serializers.CharField(write_only=True)

    class Meta:
        model = User
        fields = ('email', 'password', 'password_confirm')

    def validate_password(self, value):
        try:
            validators.validate_password(password=value, user=User)
            return value
        except exceptions.ValidationError as e:
            raise serializers.ValidationError(e)

    def save(self):
        if User.objects.filter(email=self.validated_data['email']).count() > 0:
            raise serializers.ValidationError({'message': 'User already exists'})
        user = User(
            email=self.validated_data.get('email', '').lower(),
            username=self.validated_data.get('email', '').lower(),
            is_active=True,
        )
        pass1 = self.validated_data['password']
        pass2 = self.validated_data['password_confirm']
        if pass1 != pass2:
            raise serializers.ValidationError({'password_confirm': "Passwords don't match"})
        user.set_password(pass1)
        user.save()
        return user
