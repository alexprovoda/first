from django.shortcuts import render

from user.modeldata.language_choice import UserLanguageChoice


def cabinet(request):
    if request.user.is_authenticated:
        return render(request, 'cabinet.html', {'object': request.user, 'languages': UserLanguageChoice.choices()})
    else:
        return render(request, 'login.html', {'object': ''})
