from django.shortcuts import render


def home(request):
    return render(request, 'base.html', {'object': ''})


def registration(request):
    return render(request, 'registration.html', {'object': ''})
