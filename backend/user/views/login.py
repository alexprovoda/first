from django.contrib.auth import authenticate, login
from django.http import JsonResponse
from django.shortcuts import render, redirect
from rest_framework.authtoken.models import Token


def sign_in(request):
    return render(request, 'login.html', {'object': ''})

def login_form(request):
    username = request.POST.get('username')
    password = request.POST.get('password')
    user = authenticate(request, username=username.lower(), password=password)
    if user and user.is_active:
        login(request, user)
        token, created = Token.objects.get_or_create(user=user)
        request.session['AUTHORIZATION'] = "TOKEN " + token.key
        return redirect('/')
    elif user and not user.is_active:
        return JsonResponse({'detail': 'Пользователь неактивирован. Пожалуйста, активируйте вашу учетную запись'
                                       ' с помощью ссылки в письме отправленного при регистрации.'}, status=400)
    return JsonResponse({'detail': 'Пользователь не найден или не активирован'}, status=403)
