from enum import Enum
from django.utils.translation import ugettext_lazy as _

class ChoiceEnum(Enum):
    @classmethod
    def choices(cls):
        return tuple((i, i.fullname) for i in cls)

    @classmethod
    def translated_choices(cls):
        return tuple((i, _(i.fullname)) for i in cls)

    def __str__(self):
        return self.name

    @classmethod
    def from_name(cls, name):
        return cls[name]

    @classmethod
    def try_from_name(cls, name):
        try:
            return cls[name]
        except KeyError:
            return None

    def __len__(self):
        return len(self.name)