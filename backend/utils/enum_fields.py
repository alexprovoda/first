from django.core.exceptions import ValidationError
from django.db import models


def parse_status(status_string, clazz):
    try:
        return clazz[status_string]
    except ValueError:
        raise ValidationError(_("Invalid input for a UserStatus instance"))


class EnumField(models.CharField):
    def __init__(self, *args, **kwargs):
        self.clazz = kwargs.pop('clazz', None)
        super().__init__(*args, **kwargs)

    def from_db_value(self, value, expression, connection):
        if value is None:
            return value
        return parse_status(value, self.clazz)

    def to_python(self, value):
        if isinstance(value, self.clazz):
            return value

        if value is None:
            return value

        return parse_status(value, self.clazz)

    def get_prep_value(self, value):
        if isinstance(value, self.clazz):
            return value.name

        if isinstance(value, str) or value is None:
            return value

        return value
