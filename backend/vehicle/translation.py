from modeltranslation.translator import register, TranslationOptions

from vehicle.models import Vehicle


@register(Vehicle)
class VehicleTranslationOptions(TranslationOptions):
    fields = ('name', )
