import smtplib

from django.http import JsonResponse
from django.shortcuts import render
from rest_framework import viewsets, permissions
from rest_framework.decorators import action

from app.settings import EMAIL, PASSWORD_EMAIL
from vehicle.models import Vehicle
from vehicle.serializers import VehicleSerializer


def vehicle(request):
    if request.user.is_authenticated:
        return render(request, 'add_vehicle.html', {'object': ''})
    else:
        return render(request, 'login.html', {'object': ''})


def catalog_vehicle(request):
    if request.user.is_authenticated:
        vehicles = Vehicle.objects.all()
        return render(request, 'catalog_vehicle.html', {'objects': vehicles})
    else:
        return render(request, 'login.html', {'object': ''})


class VehicleViews(viewsets.ModelViewSet):
    serializer_class = VehicleSerializer
    permission_classes = (permissions.IsAuthenticated,)
    queryset = Vehicle.objects.all()

    @action(detail=True, methods=['POST', ], permission_classes=(permissions.IsAuthenticated,),
            name='add_user_vehicles')
    def add_user_vehicles(self, *args, **kwargs):
        user = self.request.user
        auto = self.request.data.get('auto_id', None)
        if user.is_authenticated:
            if auto:
                smtpObj = smtplib.SMTP('smtp.gmail.com', 587)
                smtpObj.starttls()
                smtpObj.login(EMAIL, PASSWORD_EMAIL)
                smtpObj.sendmail(EMAIL, user.email, f"Успешное добавление авто")
                smtpObj.quit()
                user.vehicles.add(auto)
                user.save()
                return JsonResponse({"status": True})
        return JsonResponse({"status": False})
