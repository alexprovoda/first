from django.contrib import admin
from modeltranslation.admin import TabbedTranslationAdmin

from .models import Vehicle


@admin.register(Vehicle)
class VehicleAdmin(TabbedTranslationAdmin):
    list_display = ['name', 'created_at', 'add_system_at']
