from django.utils import translation
from rest_framework import serializers

from user.modeldata.language_choice import UserLanguageChoice
from vehicle.models import Vehicle


class VehicleSerializer(serializers.ModelSerializer):
    name = serializers.SerializerMethodField()

    def get_name(self, obj):
        import locale
        lang = self.context['request'].user.language.code
        try:
            if lang == UserLanguageChoice.ENGLISH.code:
                translation.activate('en')
                locale.setlocale(locale.LC_ALL, 'en_US.utf8')
            else:
                translation.activate('ru')
                locale.setlocale(locale.LC_ALL, 'ru_RU.utf8')
            name_language = obj.name
            name_res = translation.gettext(name_language).title()
            return name_res
        except:
            if lang == UserLanguageChoice.ENGLISH:
                name_language = obj.name_en
            else:
                name_language = obj.name_ru
            return name_language

    class Meta:
        model = Vehicle
        fields = (
            'id',
            'name',
            'created_at',
        )
        read_only = (
            'add_system_at',
        )
