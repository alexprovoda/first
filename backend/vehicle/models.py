from django.core.validators import MaxValueValidator, MinValueValidator
from django.db import models


class Vehicle(models.Model):
    name = models.CharField(max_length=255, verbose_name='Имя')
    created_at = models.PositiveIntegerField(verbose_name='Год выпуска', validators=[MaxValueValidator(2025), MinValueValidator(1970)])
    add_system_at = models.DateTimeField(auto_created=True, verbose_name='Дата добавления в систему', blank=True,
                                         null=True)

    def __str__(self):
        return self.name

    class Meta:
        verbose_name = 'Транспорт'
        verbose_name_plural = 'Транспорт'
